/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.ws;

import net.ihe.gazelle.metadata.application.ConsumedInterfaceProvider;
import net.ihe.gazelle.metadata.application.ProvidedInterfaceProvider;
import net.ihe.gazelle.metadata.application.ServiceBuilder;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.interlay.MetadataServiceProviderImpl;
import net.ihe.gazelle.metadata.interlay.dto.ServiceDTO;
import net.ihe.gazelle.metadata.mock.ConsumedInterfaceProviderMock;
import net.ihe.gazelle.metadata.mock.ProvidedInterfaceProviderMock;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.IOException;

import static org.testng.AssertJUnit.assertEquals;

public class MetadataServiceControllerImplTest {
    private MetadataServiceController metadataServiceController;
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        environmentVariables.set("GZL_TEST_K8S_ID", "version-test-azerty1234-abc12");
        metadataServiceController = new MetadataServiceControllerImpl(new MetadataServiceProviderImpl());
    }

    @Test
    public void getMetadataTest() throws IOException {
        ConsumedInterfaceProvider consumedInterfaceProvider =new ConsumedInterfaceProviderMock();
        ProvidedInterfaceProvider providedInterfaceProvider = new ProvidedInterfaceProviderMock();
        Service service = new ServiceBuilder()
                .setVersion("1.0.0-SNAPSHOT")
                .setName("version-test")
                .setInstanceId("azerty1234")
                .setReplicaId("abc12")
                .setProvidedInterfaces(providedInterfaceProvider.getProvidedInterfaces())
                .setConsumedInterfaces(consumedInterfaceProvider.getConsumedInterfaces())
                .build();
        ObjectMapper mapper = new ObjectMapper();
        assertEquals(mapper.writeValueAsString(new ServiceDTO(service)), metadataServiceController.getMetadata().getEntity());
    }


}
