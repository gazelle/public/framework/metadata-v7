/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.ws;

import net.ihe.gazelle.metadata.interlay.MetadataServiceProviderImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.testng.AssertJUnit.assertEquals;

public class VersionControllerImplTest {
    private final VersionController versionController = new VersionControllerImpl(new MetadataServiceProviderImpl());
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Before
    public void setUp() {
        environmentVariables.set("GZL_TEST_K8S_ID", "version-test-azerty1234-abc12");
    }

    @Test
    public void getVersionTest() {
        assertEquals("1.0.0-SNAPSHOT", versionController.getVersion());
    }

    @Test
    public void getVersionTestXML() {
        String expected = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<version>1.0.0-SNAPSHOT</version>";
        assertEquals(expected, versionController.getVersionXml());
    }

}