/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay;

import net.ihe.gazelle.metadata.application.ConsumedInterfaceProvider;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.application.ProvidedInterfaceProvider;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.mock.ConsumedInterfaceProviderMock;
import net.ihe.gazelle.metadata.mock.ProvidedInterfaceProviderMock;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static org.junit.Assert.*;

public class MetadataServiceProviderImplTest {

    private MetadataServiceProvider metadataServiceProvider;

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void getMetadataTest() {
        environmentVariables.set("GZL_TEST_K8S_ID", "version-test-azerty1234-abc12");
        metadataServiceProvider = new MetadataServiceProviderImpl();

        Service service = metadataServiceProvider.getMetadata();
        assertEquals("version-test", service.getName());
        assertEquals("1.0.0-SNAPSHOT", service.getVersion());
        assertEquals("azerty1234", service.getInstanceId());
        assertEquals("abc12", service.getReplicaId());
        ProvidedInterfaceProvider providedInterfaceProvider = new ProvidedInterfaceProviderMock();
        assertTrue(providedInterfaceProvider.getProvidedInterfaces().containsAll(service.getProvidedInterfaces()));
        ConsumedInterfaceProvider consumedInterfaceProvider = new ConsumedInterfaceProviderMock();
        assertTrue(consumedInterfaceProvider.getConsumedInterfaces().containsAll(service.getConsumedInterfaces()));

    }

    @Test(expected = IllegalStateException.class)
    public void getMetadataBadK8STest() {
        environmentVariables.set("GZL_TEST_K8S_ID", "version-test-bad-k8s");
        metadataServiceProvider = new MetadataServiceProviderImpl();
        metadataServiceProvider.getMetadata();
        fail("metadataServiceProvider should have thrown an exception");
    }

}