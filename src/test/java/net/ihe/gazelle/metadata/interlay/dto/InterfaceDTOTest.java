/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay.dto;

import net.ihe.gazelle.metadata.application.InterfaceBuilder;
import net.ihe.gazelle.metadata.domain.*;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class InterfaceDTOTest {

    @Test
    public void interfaceDTOTest(){

        AuthzScope scopeTest = new AuthzScope();
        scopeTest.setName("scope:test");
        scopeTest.setDescription("This is scope for test");

        AuthzScope scopeDev = new AuthzScope();
        scopeDev.setName("scope:dev");
        scopeDev.setDescription("This is scope for dev");
        RestBinding binding = new RestBinding();
        binding.setServiceUrl("www.fakeservice.url");
        Interface anInterface = new InterfaceBuilder()
                .setInterfaceName("interface")
                .setInterfaceVersion("1.0.0-SNAPSHOT")
                .setRequired(false)
                .setBindings(Collections.<Binding>singletonList(binding))
                .setAuthzScopes(Arrays.asList(scopeTest,scopeDev))
                .setSecuredMethods(Collections.singletonList(SecuredMethod.M2M))
                .build();
        InterfaceDTO interfaceDTO = new InterfaceDTO(anInterface);

        assertEquals(anInterface.getInterfaceName(),interfaceDTO.getInterfaceName());
        assertEquals(anInterface.getInterfaceVersion(),interfaceDTO.getInterfaceVersion());
        assertEquals(anInterface.getAuthzScopes(),interfaceDTO.getAuthzScopes());
        assertEquals(anInterface.getSecuredMethods(),interfaceDTO.getSecuredMethods());
        assertEquals(anInterface.isRequired(),interfaceDTO.isRequired());
        assertEquals(anInterface.isSecured(),interfaceDTO.isSecured());
        assertEquals(anInterface.getBindings(),interfaceDTO.getBindings());
        assertEquals(anInterface,interfaceDTO.getInterface());
    }
}
