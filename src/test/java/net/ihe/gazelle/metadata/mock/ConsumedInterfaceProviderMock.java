/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.mock;

import net.ihe.gazelle.metadata.application.ConsumedInterfaceProvider;
import net.ihe.gazelle.metadata.application.InterfaceBuilder;
import net.ihe.gazelle.metadata.domain.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ConsumedInterfaceProviderMock implements ConsumedInterfaceProvider {

    @Override
    public List<Interface> getConsumedInterfaces() {
        RestBinding binding = new RestBinding();
        binding.setServiceUrl("https://fakeurel.consumed");

        AuthzScope scopeTest = new AuthzScope();
        scopeTest.setName("consumed:test");
        scopeTest.setDescription("This is scope for test");

        AuthzScope scopeDev = new AuthzScope();
        scopeDev.setName("consumed:dev");
        scopeDev.setDescription("This is scope for dev");

        Interface anInterface = new InterfaceBuilder()
                .setInterfaceName("interfaceConsumed")
                .setInterfaceVersion("1.0.0-SNAPSHOT")
                .setRequired(false)
                .setBindings(Collections.<Binding>singletonList(binding))
                .setAuthzScopes(Arrays.asList(scopeTest, scopeDev))
                .setSecuredMethods(Collections.singletonList(SecuredMethod.M2M))
                .build();
        return Collections.singletonList(anInterface);
    }
}
