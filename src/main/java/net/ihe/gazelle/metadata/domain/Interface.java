/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Interface {

    private String interfaceName;
    private String interfaceVersion;
    private boolean required;
    private boolean secured;
    private List<AuthzScope> authzScopes = new ArrayList<>();
    private List<SecuredMethod> securedMethods = new ArrayList<>();
    private List<Binding> bindings = new ArrayList<>();

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        if (interfaceName == null)
            throw new IllegalArgumentException("Interface name cannot be null");
        this.interfaceName = interfaceName;
    }

    public String getInterfaceVersion() {
        return interfaceVersion;
    }

    public void setInterfaceVersion(String interfaceVersion) {
        this.interfaceVersion = interfaceVersion;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public List<AuthzScope> getAuthzScopes() {
        return new ArrayList<>(authzScopes);
    }

    public void setAuthzScopes(List<AuthzScope> authzScopes) {
        this.authzScopes = authzScopes != null ?
                new ArrayList<>(authzScopes) :
                new ArrayList<AuthzScope>();
    }

    public boolean isSecured() {
        return secured;
    }

    public void setSecured(boolean secured) {
        this.secured = secured;
    }

    public List<SecuredMethod> getSecuredMethods() {
        return new ArrayList<>(securedMethods);
    }

    public void setSecuredMethods(List<SecuredMethod> securedMethods) {
        this.securedMethods = securedMethods != null ?
                new ArrayList<>(securedMethods) :
                new ArrayList<SecuredMethod>();
    }

    public List<Binding> getBindings() {
        return new ArrayList<>(bindings);
    }

    public void setBindings(List<Binding> bindings) {
        this.bindings = bindings != null ?
                new ArrayList<>(bindings) :
                new ArrayList<Binding>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interface that = (Interface) o;
        return required == that.required
                && secured == that.secured
                && Objects.equals(interfaceName, that.interfaceName)
                && Objects.equals(interfaceVersion, that.interfaceVersion)
                && Objects.equals(authzScopes, that.authzScopes)
                && Objects.equals(securedMethods, that.securedMethods)
                && Objects.equals(bindings, that.bindings);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                interfaceName,
                interfaceVersion,
                required,
                secured,
                authzScopes,
                securedMethods,
                bindings
        );
    }
}
