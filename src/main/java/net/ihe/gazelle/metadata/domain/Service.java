/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Service {
    private String name;
    private String instanceId;
    private String replicaId;
    private String version;
    private List<Interface> providedInterfaces = new ArrayList<>();
    private List<Interface> consumedInterfaces = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getReplicaId() {
        return replicaId;
    }

    public void setReplicaId(String replicaId) {
        this.replicaId = replicaId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<Interface> getProvidedInterfaces() {
        return new ArrayList<>(providedInterfaces);
    }

    public void setProvidedInterfaces(List<Interface> providedInterfaces) {
        this.providedInterfaces = providedInterfaces != null ?
                new ArrayList<>(providedInterfaces) :
                new ArrayList<Interface>();
    }

    public List<Interface> getConsumedInterfaces() {
        return new ArrayList<>(consumedInterfaces);
    }

    public void setConsumedInterfaces(List<Interface> consumedInterfaces) {
        this.consumedInterfaces = consumedInterfaces != null ?
                new ArrayList<>(consumedInterfaces) :
                new ArrayList<Interface>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Service service = (Service) o;
        return Objects.equals(name, service.name)
                && Objects.equals(instanceId, service.instanceId)
                && Objects.equals(replicaId, service.replicaId)
                && Objects.equals(version, service.version)
                && Objects.equals(providedInterfaces, service.providedInterfaces)
                && Objects.equals(consumedInterfaces, service.consumedInterfaces);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
                name,
                instanceId,
                replicaId,
                version,
                providedInterfaces,
                consumedInterfaces
        );
    }
}
