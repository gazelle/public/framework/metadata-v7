/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.domain;

import java.util.Objects;

public class SoapBinding implements Binding{

    private String wsdlUrl;

    public String getWsdlUrl() {
        return wsdlUrl;
    }

    public SoapBinding setWsdlUrl(String wsdlUrl) {
        this.wsdlUrl = wsdlUrl;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SoapBinding that = (SoapBinding) o;
        return Objects.equals(wsdlUrl, that.wsdlUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wsdlUrl);
    }
}
