/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.ws;

import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Stateless
@AutoCreate
@Name("versionController")
public class VersionControllerImpl implements VersionController {

    @In
    MetadataServiceProvider metadataServiceProvider;

    public VersionControllerImpl() {
    }

    public VersionControllerImpl(MetadataServiceProvider metadataServiceProvider) {
        this.metadataServiceProvider = metadataServiceProvider;
    }

    @GET
    @Path("/")
    @Produces("application/xml")
    public String getVersionXml() {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "<version>" + getVersion() + "</version>";
    }

    public String getVersion() {
        return metadataServiceProvider.getMetadata().getVersion();
    }
}