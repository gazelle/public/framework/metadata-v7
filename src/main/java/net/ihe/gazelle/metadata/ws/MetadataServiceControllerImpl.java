/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.ws;

import net.ihe.gazelle.metadata.interlay.dto.ServiceDTO;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Stateless
@AutoCreate
@Name("metadataServiceController")
public class MetadataServiceControllerImpl implements MetadataServiceController {

    @In
    MetadataServiceProvider metadataServiceProvider;

    public MetadataServiceControllerImpl(){
        //empty constructor for injection
    }

    public MetadataServiceControllerImpl(MetadataServiceProvider metadataServiceProvider){
        this.metadataServiceProvider = metadataServiceProvider;
    }
    @Override
    public Response getMetadata() {
        try {
            ObjectMapper mapper = new ObjectMapper();

            return Response.ok(mapper.writeValueAsString(new ServiceDTO(metadataServiceProvider.getMetadata()))).build();
        } catch (RuntimeException | IOException e) {
            return Response.serverError().build();
        }
    }
}
