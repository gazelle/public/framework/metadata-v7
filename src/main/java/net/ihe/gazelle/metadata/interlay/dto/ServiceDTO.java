/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay.dto;

import net.ihe.gazelle.metadata.domain.Interface;
import net.ihe.gazelle.metadata.domain.Service;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.List;
@JsonPropertyOrder({"name", "version", "instanceId", "replicaId", "providedInterfaces", "consumedInterfaces"})
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ServiceDTO {

    private final Service service;
    private final List<InterfaceDTO> providedInterfaceDTOS = new ArrayList<>();
    private final List<InterfaceDTO> consumedInterfaceDTOS = new ArrayList<>();


    public ServiceDTO(Service service) {
        this.service = service;
        if (service.getProvidedInterfaces() != null && !service.getProvidedInterfaces().isEmpty()) {
            for (Interface anInterface : service.getProvidedInterfaces()) {
                providedInterfaceDTOS.add(new InterfaceDTO(anInterface));
            }
        }
        if (service.getConsumedInterfaces() != null && !service.getConsumedInterfaces().isEmpty()) {
            for (Interface anInterface : service.getConsumedInterfaces()) {
                consumedInterfaceDTOS.add(new InterfaceDTO(anInterface));
            }
        }
    }


    @JsonProperty("name")
    public String getName() {
        return service.getName();
    }

    @JsonProperty("version")
    public String getVersion() {
        return service.getVersion();
    }

    @JsonProperty("instanceId")
    public String getInstanceId() {
        return service.getInstanceId();
    }

    @JsonProperty("replicaId")
    public String getReplicaId() {
        return service.getReplicaId();
    }

    @JsonProperty("providedInterfaces")
    public List<InterfaceDTO> getProvidedInterfaces() {
        return new ArrayList<>(providedInterfaceDTOS);
    }
    @JsonProperty("consumedInterfaces")
    public List<InterfaceDTO> getConsumedInterfaces() {
        return new ArrayList<>(consumedInterfaceDTOS);
    }

}
