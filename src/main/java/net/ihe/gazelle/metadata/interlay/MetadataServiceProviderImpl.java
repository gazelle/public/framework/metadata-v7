/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay;

import net.ihe.gazelle.metadata.domain.Interface;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.application.ConsumedInterfaceProvider;
import net.ihe.gazelle.metadata.application.ProvidedInterfaceProvider;
import net.ihe.gazelle.metadata.application.ServiceBuilder;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MetadataServiceProviderImpl implements MetadataServiceProvider {
    private static final Logger LOG = LoggerFactory.getLogger(MetadataServiceProviderImpl.class);

    private static final String K8S_ID_REGEXP = "^([a-z0-9\\-]+)\\-([a-z0-9]{5,})\\-([a-z0-9]{3,})$";
    private static final Pattern pattern = Pattern.compile(K8S_ID_REGEXP);
    private static final String GZL_SERVICE_K8S_ID_VARNAME = "gzl.service.k8s.id.varname";
    private static final int GROUP_INSTANCE_ID = 2;
    private static final int GROUP_REPLICA_ID = 3;
    private String k8sId;
    private String instanceId;
    private String replicaId;

    private List<Interface> providedInterfaces = new ArrayList<>();
    private List<Interface> consumedInterfaces = new ArrayList<>();

    private final Properties properties;

    public MetadataServiceProviderImpl() {
        initProvidedInterfaces();
        initConsumedInterfaces();
        properties = initProperties();
    }


    @Override
    public Service getMetadata() {
        if (instanceId == null || replicaId == null)
            parseK8sId();

        return new ServiceBuilder()
                .setName(getName())
                .setVersion(getVersion())
                .setInstanceId(instanceId)
                .setReplicaId(replicaId)
                .setProvidedInterfaces(providedInterfaces)
                .setConsumedInterfaces(consumedInterfaces)
                .build();
    }

    private String getName() {
        return properties.getProperty("gzl.service.name");
    }


    private String getVersion() {
        return properties.getProperty("gzl.service.version");
    }


    private Properties initProperties() {
        Properties propertiesToReturn = new Properties();
        try (InputStream gzlVersionProperties = getClass().getResourceAsStream("/gzl.metadata.properties")) {
            propertiesToReturn.load(gzlVersionProperties);
            k8sId = System.getenv(propertiesToReturn.getProperty(GZL_SERVICE_K8S_ID_VARNAME));
        } catch (IOException e) {
            LOG.error("Failed to load properties", e);
        }
        return propertiesToReturn;
    }

    private void initConsumedInterfaces() {
        ServiceLoader<ConsumedInterfaceProvider> consumedInterfaceProviderServiceLoader =
                ServiceLoader.load(ConsumedInterfaceProvider.class);

        for (ConsumedInterfaceProvider current : consumedInterfaceProviderServiceLoader) {
            consumedInterfaces.addAll(current.getConsumedInterfaces());
        }
    }

    private void initProvidedInterfaces() {
        ServiceLoader<ProvidedInterfaceProvider> providedInterfaceProviderServiceLoader =
                ServiceLoader.load(ProvidedInterfaceProvider.class);

        for (ProvidedInterfaceProvider current : providedInterfaceProviderServiceLoader) {
            providedInterfaces.addAll(current.getProvidedInterfaces());
        }
    }

    private void parseK8sId() {
        if (k8sId != null) {
            Matcher matcher = pattern.matcher(k8sId);
            if (matcher.find()) {
                instanceId = matcher.group(GROUP_INSTANCE_ID);
                replicaId = matcher.group(GROUP_REPLICA_ID);
            } else {
                throw new IllegalStateException(
                        "Unable to parse instanceId and replicaId from k8s id. Check for "
                                + GZL_SERVICE_K8S_ID_VARNAME
                                + " property variable."
                );
            }
        }
    }
}
