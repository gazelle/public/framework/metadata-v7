/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay.dto;

import net.ihe.gazelle.metadata.domain.AuthzScope;
import net.ihe.gazelle.metadata.domain.Binding;
import net.ihe.gazelle.metadata.domain.Interface;
import net.ihe.gazelle.metadata.domain.SecuredMethod;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonPropertyOrder({"interfaceName", "interfaceVersion", "required", "bindings", "secured", "securedMethods", "authzScopes"})
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class InterfaceDTO {

    private final Interface anInterface;

    public InterfaceDTO(Interface anInterface) {
        this.anInterface = anInterface;
    }

    @JsonProperty("interfaceName")
    public String getInterfaceName() {
        return anInterface.getInterfaceName();
    }

    @JsonProperty("interfaceVersion")
    public String getInterfaceVersion() {
        return anInterface.getInterfaceVersion();
    }

    @JsonProperty("required")
    public boolean isRequired() {
        return anInterface.isRequired();
    }

    @JsonProperty("secured")
    public boolean isSecured() {
        return anInterface.isSecured();
    }

    @JsonProperty("authzScopes")
    public List<AuthzScope> getAuthzScopes() {
        return anInterface.getAuthzScopes();
    }

    @JsonProperty("securedMethods")
    public List<SecuredMethod> getSecuredMethods() {
        return anInterface.getSecuredMethods();
    }

    @JsonProperty("bindings")
    public List<Binding> getBindings() {
        return anInterface.getBindings();
    }

    @JsonIgnore
    public Interface getInterface() {
        return anInterface;
    }

}
