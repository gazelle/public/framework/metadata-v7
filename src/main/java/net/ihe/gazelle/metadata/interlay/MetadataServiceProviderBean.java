/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.interlay;

import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Scope(ScopeType.APPLICATION)
@AutoCreate
@Name("metadataServiceProvider")
public class MetadataServiceProviderBean implements MetadataServiceProvider {
    private final MetadataServiceProvider metadataServiceProvider;

    public MetadataServiceProviderBean() {
        metadataServiceProvider = new MetadataServiceProviderImpl();
    }

    @Override
    public Service getMetadata() {
        return metadataServiceProvider.getMetadata();
    }
}
