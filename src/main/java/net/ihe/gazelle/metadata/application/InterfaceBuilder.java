/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.application;

import net.ihe.gazelle.metadata.domain.AuthzScope;
import net.ihe.gazelle.metadata.domain.Binding;
import net.ihe.gazelle.metadata.domain.Interface;
import net.ihe.gazelle.metadata.domain.SecuredMethod;

import java.util.ArrayList;
import java.util.List;

public class InterfaceBuilder {

    private String interfaceName;
    private String interfaceVersion;
    private boolean required;
    private boolean secured;
    private List<AuthzScope> authzScopes = new ArrayList<>();
    private List<SecuredMethod> securedMethods = new ArrayList<>();
    private List<Binding> bindings = new ArrayList<>();

    public InterfaceBuilder setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
        return this;
    }

    public InterfaceBuilder setInterfaceVersion(String interfaceVersion) {
        this.interfaceVersion = interfaceVersion;
        return this;
    }

    public InterfaceBuilder setRequired(boolean required) {
        this.required = required;
        return this;
    }

    public InterfaceBuilder setSecured(boolean secured) {
        this.secured = secured;
        return this;
    }

    public InterfaceBuilder setAuthzScopes(List<AuthzScope> authzScopes) {
        this.authzScopes = authzScopes != null ?
                new ArrayList<>(authzScopes) :
                new ArrayList<AuthzScope>();
        return this;
    }

    public InterfaceBuilder setSecuredMethods(List<SecuredMethod> securedMethods) {
        this.securedMethods = securedMethods != null ?
                new ArrayList<>(securedMethods) :
                new ArrayList<SecuredMethod>();
        return this;
    }

    public InterfaceBuilder setBindings(List<Binding> bindings) {

        this.bindings = bindings != null ?
                new ArrayList<>(bindings) :
                new ArrayList<Binding>();
        return this;
    }

    public Interface build() {
        if (interfaceName == null)
            throw new IllegalStateException("Interface name should not be null");

        Interface anInterface = new Interface();
        anInterface.setInterfaceName(interfaceName);
        anInterface.setInterfaceVersion(interfaceVersion);
        anInterface.setSecured(secured);
        anInterface.setRequired(required);
        anInterface.setAuthzScopes(authzScopes);
        anInterface.setSecuredMethods(securedMethods);
        anInterface.setBindings(bindings);

        if (secured && securedMethods.isEmpty())
            throw new IllegalStateException("Interface is secured but does not have secured method(s)");

        return anInterface;
    }
}
