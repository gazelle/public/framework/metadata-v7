/*
 * Copyright 2023 IHE International
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package net.ihe.gazelle.metadata.application;

import net.ihe.gazelle.metadata.domain.Interface;
import net.ihe.gazelle.metadata.domain.Service;

import java.util.ArrayList;
import java.util.List;

public class ServiceBuilder {

    private String name;
    private String instanceId;
    private String replicaId;
    private String version;
    private List<Interface> providedInterfaces = new ArrayList<>();
    private List<Interface> consumedInterfaces = new ArrayList<>();

    public ServiceBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ServiceBuilder setInstanceId(String instanceId) {
        this.instanceId = instanceId;
        return this;
    }

    public ServiceBuilder setReplicaId(String replicaId) {
        this.replicaId = replicaId;
        return this;
    }

    public ServiceBuilder setVersion(String version) {
        this.version = version;
        return this;
    }

    public ServiceBuilder setProvidedInterfaces(List<Interface> providedInterfaces) {
        this.providedInterfaces = providedInterfaces != null ?
                new ArrayList<>(providedInterfaces) :
                new ArrayList<Interface>();
        return this;
    }

    public ServiceBuilder addProvidedInterface(Interface providedInterface) {
        if (providedInterface != null)
            providedInterfaces.add(providedInterface);
        return this;
    }

    public ServiceBuilder setConsumedInterfaces(List<Interface> consumedInterfaces) {
        this.consumedInterfaces = consumedInterfaces != null ?
                new ArrayList<>(consumedInterfaces) :
                new ArrayList<Interface>();
        return this;
    }

    public ServiceBuilder addConsumedInterface(Interface consumedInterfaces) {
        if (consumedInterfaces != null)
            providedInterfaces.add(consumedInterfaces);
        return this;
    }


    public Service build() {
        Service service = new Service();
        service.setName(name);
        service.setVersion(version);
        service.setInstanceId(instanceId);
        service.setReplicaId(replicaId);
        service.setProvidedInterfaces(providedInterfaces);
        service.setConsumedInterfaces(consumedInterfaces);

        return service;
    }
}
