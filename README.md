# Metadata v7

The goal of this library is to provide metadata of a service during runtime.
It also offers a REST endpoint to these metadata.

<!-- TOC -->
* [Metadata v7](#metadata-v7)
  * [Build and tests](#build-and-tests)
  * [Setup](#setup)
    * [Maven import](#maven-import)
    * [Properties](#properties)
    * [REST web-service](#rest-web-service)
  * [Usage of metadata](#usage-of-metadata)
<!-- TOC -->

## Build and tests

The project must be build using JDK-8 and run on a JDK-7 environment.

```shell
mvn clean install
```

## Setup

### Maven import

To use Metadata v7 in an application you must add the dependency seen below :

```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>metadata-v7</artifactId>
    <version>${metadata.v7.version}</version>
    <type>ejb</type>
</dependency>
```

__Metadata v7__ depends on JBoss Seam framework to work.

If packaged in an EAR, it must be declared as `ejbModule` in your EAR pom as seen below:

```xml
<ejbModule>
  <groupId>net.ihe.gazelle</groupId>
  <artifactId>metadata-v7</artifactId>
  <uri>metadata-v7.jar</uri>
</ejbModule>
```

### Properties

Each application using "Metadata-v7" must have the properties, seen below, in the `gzl.metadata.properties`
file located in `my-service-ejb/src/resources/` .

```properties
gzl.service.name=my-service
gzl.service.version=${build.version}
gzl.service.k8s.id.varname=GZL_XXXXXXX_K8S_ID
```

* `gzl.service.name`: Name of the service
* `gzl.service.version` Build version of the service. (This value can be injected at build time
  using Maven resource filtering).
* `gzl.service.k8s.id.varname`: Name of the environment variable that holds the k8s id for your service. This name should
  respect the format GZL_XXXXXXX_K8S_ID with XXXXXXX being the service name uppercase.
  Moreover, the value of the k8s id is the instance identification of the service with the
  format `name-aaaaaaaaaa-bbbbb`, where `name` is the service's name (alphabetic and dash `-`
  characters allowed), `aaaaaaaaaa` is the instance id (10 alphanumeric chars), and `bbbbb` is the
  replica id (5 alphanumeric chars). This value can be injected at runtime with the environment
  variable `GZL_XXXXXXX_K8S_ID` (replace XXXXXXX by the service name uppercase with underscores).

This could give for example:

```properties
gzl.service.name=gazelle-webservice-tester
gzl.service.version=${build.version}
gzl.service.k8s.id.varname=GZL_WEBSERVICE_TESTER_K8S_ID
```

### REST web-service

If you want to expose the metadata and the version of your service as a REST webservice, you need to add the
following lines in the `components` tag of the Seam file `components.xml` (in `WEB-INF/` or
`META-INF/`).

```xml
<component class="net.ihe.gazelle.metadata.ws.VersionControllerImpl"
           jndi-name="java:module/metadata-v7/VersionControllerImpl"/>
```

```xml
<component class="net.ihe.gazelle.metadata.ws.MetadataServiceControllerImpl"
           jndi-name="java:module/metadata-v7/MetadataServiceControllerImpl"/>
```

You also need to add the following lines in the `web.xml` file (in `WEB-INF/` or `META-INF/`).

```xml
<context-param>
    <param-name>resteasy.jndi.resources</param-name>
    <param-value>java:app/metadata-v7/VersionControllerImpl,java:app/metadata-v7/MetadataServiceControllerImpl</param-value>
</context-param>
```

With these configurations, you can access the REST webservices at the following URLs :

```bash
# The version of the service in XML format
https://yourdomain/yourservice/rest/version

# The metadata of the service in JSON format
https://yourdomain/yourservice/rest/metadata
```

## Usage of metadata

It's possible to retrieve the metadata of a service at runtime using `MetadataServiceProvider`.

Among the information provided by the service, you can find :
- the name of the service
- the version of the service
- the instance id of the service
- the replica id of the service
- the provided and consumed interfaces of the service

It is possible to inject the Seam bean `metadataServiceProvider` to get service information with the code below:

```java
@In(value = "metadataServiceProvider")
MetadataServiceProvider metadataServiceProvider;
```

If your application provides Interfaces, you can implement the interface [
ProvidedInterfaceProvider](src/main/java/net/ihe/gazelle/metadata/application/ProvidedInterfaceProvider.java)

If your application consumes Interfaces, you can implement the interface [
ConsumedInterfaceProvider](src/main/java/net/ihe/gazelle/metadata/application/ConsumedInterfaceProvider.java)
in your project.

Then you will need to add in **resources/META-INF/services** the file
**net.ihe.gazelle.metadata.application.ProvidedInterfaceProvider**. In this file, you need to add the fully-qualified
name of your implementation. If you have multiple implementations, be sure to separate them with a new line.

You can find examples here for [interface implementation](src/test/java/net/ihe/gazelle/metadata/mock) and for here for
[SPI](src/test/resources/META-INF/services).